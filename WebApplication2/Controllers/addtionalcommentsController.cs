﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    [Authorize]
    public class addtionalcommentsController : Controller
    {
        private academy db = new academy();

        // GET: addtionalcomments
        public ActionResult Index()
        {
            var addtionalcomment = db.addtionalcomment.Include(a => a.hero).Include(a => a.item);
            return View(addtionalcomment.ToList());
        }

        // GET: addtionalcomments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            addtionalcomment addtionalcomment = db.addtionalcomment.Find(id);
            if (addtionalcomment == null)
            {
                return HttpNotFound();
            }
            return View(addtionalcomment);
        }

        // GET: addtionalcomments/Create
        public ActionResult Create()
        {
            ViewBag.heroid = new SelectList(db.hero, "id", "name");
            ViewBag.itemid = new SelectList(db.item, "id", "discription");
            return View();
        }

        // POST: addtionalcomments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "heroid,itemid,content")] addtionalcomment addtionalcomment)
        {
            if (ModelState.IsValid)
            {
                db.addtionalcomment.Add(addtionalcomment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.heroid = new SelectList(db.hero, "id", "name", addtionalcomment.heroid);
            ViewBag.itemid = new SelectList(db.item, "id", "discription", addtionalcomment.itemid);
            return View(addtionalcomment);
        }

        // GET: addtionalcomments/Edit/5
        public ActionResult Edit(int? heroid, int? itemid)
        {
            if (heroid == null || itemid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            addtionalcomment addtionalcomment = db.addtionalcomment.Find(heroid,itemid);
            if (addtionalcomment == null)
            {
                return HttpNotFound();
            }
            ViewBag.heroid = new SelectList(db.hero, "id", "name", addtionalcomment.heroid);
            ViewBag.itemid = new SelectList(db.item, "id", "discription", addtionalcomment.itemid);
            return View(addtionalcomment);
        }

        // POST: addtionalcomments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "heroid,itemid,content")] addtionalcomment addtionalcomment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(addtionalcomment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.heroid = new SelectList(db.hero, "id", "name", addtionalcomment.heroid);
            ViewBag.itemid = new SelectList(db.item, "id", "discription", addtionalcomment.itemid);
            return View(addtionalcomment);
        }

        // GET: addtionalcomments/Delete/5
        public ActionResult Delete(int? heroid, int? itemid)
        {
            if (heroid == null || itemid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            addtionalcomment addtionalcomment = db.addtionalcomment.Find(heroid, itemid);
            if (addtionalcomment == null)
            {
                return HttpNotFound();
            }
            return View(addtionalcomment);
        }

        // POST: addtionalcomments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int heroid, int itemid)
        {
            addtionalcomment addtionalcomment = db.addtionalcomment.Find(heroid, itemid);
            db.addtionalcomment.Remove(addtionalcomment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
