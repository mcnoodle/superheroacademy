﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    [Authorize]
    public class ScoringsController : Controller
    {
        private academy db = new academy();

        // GET: Scorings
        public ActionResult Index()
        {
            var scoring = db.scoring.Include(s => s.domain);
            return View(scoring.ToList());
        }

        // GET: Scorings/Details/5
        public ActionResult Details(int? itemid, string domain, int? score )
        {
            if (itemid == null || domain==null || score==null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            scoring scoring = db.scoring.Find(itemid,domain,score);
            if (scoring == null)
            {
                return HttpNotFound();
            }
            return View(scoring);
        }

        // GET: Scorings/Create
        public ActionResult Create()
        {
            ViewBag.itemid = new SelectList(db.domain, "itemid", "itemid");
            ViewBag.domainName= new SelectList(db.domain, "name", "name");
            return View();
        }

        // POST: Scorings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "itemid,domainName,discription,score")] scoring scoring)
        {
            if (ModelState.IsValid)
            {
                db.scoring.Add(scoring);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.itemid = new SelectList(db.domain, "itemid", "color", scoring.itemid);
            return View(scoring);
        }

        // GET: Scorings/Edit/5
        public ActionResult Edit(int? itemid, string domain, int? score)
        {
            if (itemid == null || domain == null || score == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            scoring scoring = db.scoring.Find(itemid, domain, score);
            if (scoring == null)
            {
                return HttpNotFound();
            }
            ViewBag.itemid = new SelectList(db.domain, "itemid", "color", scoring.itemid);
            return View(scoring);
        }

        // POST: Scorings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "itemid,domainName,discription,score")] scoring scoring)
        {
            if (ModelState.IsValid)
            {
                db.Entry(scoring).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.itemid = new SelectList(db.domain, "itemid", "color", scoring.itemid);
            return View(scoring);
        }

        // GET: Scorings/Delete/5
        public ActionResult Delete(int? itemid, string domain, int? score)
        {
            if (itemid == null || domain == null || score == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            scoring scoring = db.scoring.Find(itemid, domain, score);
            if (scoring == null)
            {
                return HttpNotFound();
            }
            return View(scoring);
        }

        // POST: Scorings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            scoring scoring = db.scoring.Find(id);
            db.scoring.Remove(scoring);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
