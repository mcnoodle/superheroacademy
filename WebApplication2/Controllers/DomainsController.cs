﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    [Authorize]
    public class DomainsController : Controller
    {
        private academy db = new academy();

        // GET: Domains
        public ActionResult Index()
        {
            var domain = db.domain.Include(d => d.item);
            return View(domain.ToList());
        }

        // GET: Domains/Details/5
        public ActionResult Details(int? itemid, string name)
        {
            if (itemid == null|| name==null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            domain domain = db.domain.Find(itemid,name);
            if (domain == null)
            {
                return HttpNotFound();
            }
            return View(domain);
        }

        // GET: Domains/Create
        public ActionResult Create()
        {
            ViewBag.itemid = new SelectList(db.item, "id", "discription");
            return View();
        }

        // POST: Domains/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "itemid,name,score,color")] domain domain)
        {
            if (ModelState.IsValid)
            {
                db.domain.Add(domain);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.itemid = new SelectList(db.item, "id", "discription", domain.itemid);
            return View(domain);
        }

        // GET: Domains/Edit/5
        public ActionResult Edit(int? itemid, string name)
        {
            if (itemid == null || name == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            domain domain = db.domain.Find(itemid, name);
            if (domain == null)
            {
                return HttpNotFound();
            }
            ViewBag.itemid = new SelectList(db.item, "id", "discription", domain.itemid);
            return View(domain);
        }

        // POST: Domains/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "itemid,name,score,color")] domain domain)
        {
            if (ModelState.IsValid)
            {
                db.Entry(domain).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.itemid = new SelectList(db.item, "id", "discription", domain.itemid);
            return View(domain);
        }

        // GET: Domains/Delete/5
        public ActionResult Delete(int? itemid, string name)
        {
            if (itemid == null || name == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            domain domain = db.domain.Find(itemid, name);
            if (domain == null)
            {
                return HttpNotFound();
            }
            return View(domain);
        }

        // POST: Domains/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int itemid, string name)
        {
            domain domain = db.domain.Find(itemid, name);
            db.domain.Remove(domain);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
