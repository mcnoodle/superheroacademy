﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    [Authorize]
    public class ScoresController : Controller
    {
        private academy db = new academy();

        // GET: Scores
        public ActionResult Index()
        {
            var score = db.score.Include(s => s.domain).Include(s => s.hero);
            return View(score.ToList());
        }

        // GET: Scores/Details/5
        public ActionResult Details(int? heroid, int? itemid, string name)
        {
            if (heroid == null || itemid == null || name == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            score score = db.score.Find(heroid, itemid, name);
            if (score == null)
            {
                return HttpNotFound();
            }
            return View(score);
        }

        // GET: Scores/Create
        public ActionResult Create()
        {
            ViewBag.itemid = new SelectList(db.domain, "itemid", "itemid");
            ViewBag.heroid = new SelectList(db.hero, "id", "name");
            ViewBag.domainName = new SelectList(db.domain, "name", "name");
            return View();
        }

        // POST: Scores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "heroid,itemid,domainName,score1")] score score)
        {
            if (ModelState.IsValid)
            {
                db.score.Add(score);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.itemid = new SelectList(db.domain, "itemid", "itemid", score.itemid);
            ViewBag.heroid = new SelectList(db.hero, "id", "name", score.heroid);
            return View(score);
        }

        // GET: Scores/Edit/5
        public ActionResult Edit(int? heroid, int? itemid, string name)
        {
            if (heroid == null|| itemid==null|| name==null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            score score = db.score.Find(heroid, itemid, name);
            if (score == null)
            {
                return HttpNotFound();
            }
            ViewBag.itemid = new SelectList(db.domain, "itemid", "itemid", score.itemid);
            ViewBag.heroid = new SelectList(db.hero, "id", "name", score.heroid);
            return View(score);
        }

        // POST: Scores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "heroid,itemid,domainName,score1")] score score)
        {
            if (ModelState.IsValid)
            {
                db.Entry(score).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.itemid = new SelectList(db.domain, "itemid", "itemid", score.itemid);
            ViewBag.heroid = new SelectList(db.hero, "id", "name", score.heroid);
            return View(score);
        }

        // GET: Scores/Delete/5
        public ActionResult Delete(int? heroid, int? itemid, string name)
        {
            if (heroid == null || itemid == null || name == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            score score = db.score.Find(heroid, itemid, name);
            if (score == null)
            {
                return HttpNotFound();
            }
            return View(score);
        }

        // POST: Scores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int heroid, int itemid, string name)
        {
            score score = db.score.Find(heroid, itemid, name);
            db.score.Remove(score);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
