﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    [Authorize]
    public class TestController : Controller
    {
        private academy db = new academy();
      //  private static int? heroid;
        // GET: Test
        public ActionResult Start(int? id)
        {
            
            if (id == null )
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            hero hero = db.hero.Find(id);
          //  item item = db.item.Find(1);
            if (hero == null)
            {
                return HttpNotFound();
            }
            ViewBag.heroid = id;
            ViewBag.Name = hero.name;
            ViewBag.Photo = "startpage.jpg";
            return View(hero);
        }
        //demo
        public ActionResult Demo(int? heroid, int? itemid)
        {
            
            if (itemid == null | heroid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            item item = db.item.Find(itemid);
            ViewBag.heroid = heroid;
            ViewBag.itemName = item.discription;
            ViewBag.Photo = "item" + itemid.ToString() + ".jpg";
            return View(item);
        }
        public ActionResult Test(int? heroid, int? itemid)
        {
            if (heroid == null || itemid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            item item = db.item.Find(itemid);
            if (item == null)
            {
                return HttpNotFound();
            }
            ViewBag.heroid = heroid;
            ViewBag.itemid = itemid;
            var domains = db.domain.Where(d => d.itemid == itemid);
                         
            return View(domains.ToList());
        }
       
       public ActionResult Record(int? heroid, int? itemid)
        {
            
            if (heroid == null || itemid == null)
            {
                System.Diagnostics.Debug.WriteLine(academy.itemNum);
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            var domains = db.domain.Where(d => d.itemid == itemid).ToList();
          
            foreach (domain domain in domains)
            {
                var score = Request[domain.name];
                if (score == null) continue;

               // int id = 1;
               // hero hero = db.hero.Find(id);
                score data = db.score.Find((int) heroid, (int) itemid, domain.name);//(from record in db.score where (record.heroid == heroid && record.itemid == itemid && record.domainName == domain.name) select record).FirstOrDefault();
               // score data=null;
                if (data == null)
                {
                    score heroscore = new score();
                    heroscore.heroid = (int)heroid;
                    heroscore.itemid = (int)itemid;
                    heroscore.domainName = domain.name;
                    heroscore.score1=int.Parse( score);
                    if (ModelState.IsValid)
                    {
                        db.score.Add(heroscore);
                    }

                }
                else
                {
                    data.score1 = int.Parse(score);
                }
            }
            var content = Request["comment:"];
            if (content != null)
            {
                addtionalcomment comment = db.addtionalcomment.Find((int)heroid, (int)itemid);
                if (comment == null)
                {
                    addtionalcomment newcomment = new addtionalcomment();
                    newcomment.heroid= (int)heroid;
                    newcomment.itemid = (int)itemid;
                    newcomment.content = content.ToString();
                    if (ModelState.IsValid)
                    {
                        db.addtionalcomment.Add(newcomment);
                    }

                }else
                {
                    comment.content= content.ToString();
                }
            }
            System.Diagnostics.Debug.WriteLine(content.ToString());
          
            db.SaveChanges();
                itemid++;
            if (itemid > academy.itemNum)
            {
                return RedirectToAction("Details", "heroes", new { id = heroid });
               
            }
            return RedirectToAction("Demo",new { heroid, itemid });
           
         }
        public ActionResult Index()
        {
            var scoring = db.scoring.Include(s => s.domain);
            return View(scoring.ToList());
        }

        // GET: Test/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            scoring scoring = db.scoring.Find(id);
            if (scoring == null)
            {
                return HttpNotFound();
            }
            return View(scoring);
        }

        // GET: Test/Create
        public ActionResult Create()
        {
            ViewBag.itemid = new SelectList(db.domain, "itemid", "color");
            return View();
        }

        // POST: Test/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "itemid,domainName,discription,score")] scoring scoring)
        {
            if (ModelState.IsValid)
            {
                db.scoring.Add(scoring);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.itemid = new SelectList(db.domain, "itemid", "color", scoring.itemid);
            return View(scoring);
        }

        // GET: Test/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            scoring scoring = db.scoring.Find(id);
            if (scoring == null)
            {
                return HttpNotFound();
            }
            ViewBag.itemid = new SelectList(db.domain, "itemid", "color", scoring.itemid);
            return View(scoring);
        }

        // POST: Test/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "itemid,domainName,discription,score")] scoring scoring)
        {
            if (ModelState.IsValid)
            {
                db.Entry(scoring).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.itemid = new SelectList(db.domain, "itemid", "color", scoring.itemid);
            return View(scoring);
        }

        // GET: Test/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            scoring scoring = db.scoring.Find(id);
            if (scoring == null)
            {
                return HttpNotFound();
            }
            return View(scoring);
        }

        // POST: Test/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            scoring scoring = db.scoring.Find(id);
            db.scoring.Remove(scoring);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
