﻿using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    [Authorize]
    public class HeroesController : Controller
    {
        private academy db = new academy();

        // GET: heroes
       
        public ActionResult Index()
        {
            var hero = db.hero;
            return View(hero.ToList());
        }

        // GET: heroes/Details/5
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int heroid = (int)id;
            var scores = db.score.Where(s=>s.heroid==heroid).ToList();
            if (scores == null)
            {
                return HttpNotFound("No such item");
            }
            ViewBag.id = id;
            string sqlbase = "Select sum(score.score) FROM score, domain WHERE score.itemid=domain.itemid AND score.domainName = domain.name AND heroid = " + heroid.ToString();
            string conditionRed = " AND color = 'red'";
            string conditionYellow = " AND color = 'yellow'";
            string conditionBlue = " AND color = 'blue'";
           
            int? scoreBlueitems = db.Database.SqlQuery<int?>(sqlbase + conditionBlue).Single();
            int? scoreYellowitems = db.Database.SqlQuery<int?>(sqlbase + conditionYellow).Single();
            int? scoreReditems = db.Database.SqlQuery<int?>(sqlbase + conditionRed).Single();
            int? redTotal= db.Database.SqlQuery<int?>("SELECT sum(score) FROM domain WHERE color='red'").Single();
            int? blueTotal = db.Database.SqlQuery<int?>("SELECT sum(score) FROM domain WHERE color='blue'").Single();
            int? yellowTotal = db.Database.SqlQuery<int?>("SELECT sum(score) FROM domain WHERE color='yellow'").Single();
            int pRed = 0;
            int pBlue = 0;
            int pYellow = 0;
            if (redTotal != 0 && redTotal != null&& scoreReditems!=null) pRed = (int)scoreReditems * 100 / (int)redTotal ;
            if (blueTotal != 0 && blueTotal != null&& scoreBlueitems!=null) pBlue = (int)scoreBlueitems * 100 / (int)blueTotal ;
            if (yellowTotal != 0 && yellowTotal != null&& scoreYellowitems!=null)
            {
               // System.Diagnostics.Debug.WriteLine(yellowTotal);
               // System.Diagnostics.Debug.WriteLine("yellowTotal:");
                pYellow = (int)(scoreYellowitems * 100 / yellowTotal);
               // System.Diagnostics.Debug.WriteLine(pYellow);
            }

            ViewBag.Red = "Total Score for \nAll RED Items = \n" + scoreReditems.ToString() + "/" + redTotal.ToString();
            ViewBag.pRed = "Percentage Score = " + pRed.ToString() + "%";
            ViewBag.Blue = "Total Score for \nAll BLUE Items = \n" + scoreBlueitems.ToString() + "/" + blueTotal.ToString();
            ViewBag.pBlue = "Percentage Score = " + pBlue.ToString() + "%";
            ViewBag.Yellow = "Total Score for \nAll YELLOW Items = \n" + scoreYellowitems.ToString() + "/" + yellowTotal.ToString();
            ViewBag.pYellow = "Percentage Score = " + pYellow.ToString() + "%";
            var ac=db.addtionalcomment.Where(m => m.heroid == heroid).ToList();
            StringBuilder comment = new StringBuilder();
            foreach( var item in ac)
            {
                comment.Append(item.itemid.ToString());
                comment.Append(". ");
                comment.Append(item.content.ToString());
                comment.Append(System.Environment.NewLine);
            }
            ViewBag.Comment = comment.ToString();

          //   System.Diagnostics.Debug.WriteLine( comment.ToString());
            return View(scores);
        }
        
      
        // GET: heroes/Create
        public ActionResult Create()
        {
            ViewBag.id = new SelectList(db.comment, "heroid", "content");
            return View();
        }
       
     

        // POST: heroes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name")] hero hero)
        {
            if (ModelState.IsValid)
            {
                db.hero.Add(hero);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id = new SelectList(db.comment, "heroid", "content", hero.id);
            return View(hero);
        }

        // GET: heroes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            hero hero = db.hero.Find(id);
            if (hero == null)
            {
                return HttpNotFound();
            }
            ViewBag.id = new SelectList(db.comment, "heroid", "content", hero.id);
            return View(hero);
        }

        // POST: heroes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name")] hero hero)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hero).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id = new SelectList(db.comment, "heroid", "content", hero.id);
            return View(hero);
        }

        // GET: heroes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            hero hero = db.hero.Find(id);
            if (hero == null)
            {
                return HttpNotFound();
            }
            return View(hero);
        }

        // POST: heroes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            hero hero = db.hero.Find(id);
            db.hero.Remove(hero);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
